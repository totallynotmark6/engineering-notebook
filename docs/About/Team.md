# About Us

We are a rookie FTC team based at Archbishop Shaw High School in Marrero, Louisiana. Our team started in the 2017-2018 school year after a few students in the Engineering class showed interest in a robotics club. We use our school’s mascot as our symbol: the eagle. Our FTC number is 13456. Our first team name was the “Enigmatic Technomancers”. Recognizing a need for a new, better name, we are now the `insert new name here`.
