# Current Team Members

## Moderators
### Mr. Benjamin DeMaris
Coach | Physics & Engineering Teacher

### Mr. Benjamin Russo
Mentor | Chemistry & Speech Teacher

## Members

### Thien Tran
Class of 2019

Years of FTC: 2
Main Role: Captain
Sub-Roles: Programmer, Mechanic, and Engineer
??? quote "Goal For The Year"
    To learn as much as I can, and to make cool things.

### Kennedy Tran
Class of 2022

Years of FTC: 2
Main Role: Sub-Captain
Sub-Roles: Programmer and Mechanic
??? quote "Goal For The Year"
    Learn.

### Jordan Singh
Class of 2022

Main Role: Head Mechanic
Sub-Roles: Engineer
Years of FTC: 2
??? quote "Goal For The Year"
    s

### Troy Simmons
Class of 2021

Main Role: Head Programmer
Sub-Roles: Engineer
Years of FTC: 1
??? quote "Goal For The Year"
    Learn and do well.

### Mark Smith
Class of 2022

Main Role: Engineering Notebook Manager 
Sub-Roles: Programmer
Years of FTC: 1
??? quote "Goal For The Year"
    Not fail.

### Jonathan Freeman
Class of 2022

Main Role: Head Driver
Sub-Roles: Engineer and Mechanic
Years of FTC: 1
??? quote "Goal For The Year"
    s

### Jason Brown
Class of 2023

Main Role: Head Engineer
Sub-Roles: Mechanic
Years of FTC: 1
??? quote "Goal For The Year"
    Make good robot.

### Lucas Faircloth
Class of 2023

Main Role: Driving Coach
Sub-Roles: Engineer
Years of FTC: 1
??? quote "Goal For The Year"
    My main goal is to make sure that the team is always working at their best.

### Noah Guidroz
Class of 2023

Main Role: Outreach Manager
Sub-Roles: Programmer
Years of FTC: 1
??? quote "Goal For The Year"
    My goal is to do the best I can on and off the field and make sure everybody else is doing the same.

### Jack Lopez
Class of 2023

Main Role: Captain's Assistant
Sub-Roles: Engineer and MEchanic
Years of FTC: 1
??? quote "Goal For The Year"
    To make sure that our captain is doing everything right.