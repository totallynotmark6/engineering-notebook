# To-Do

!!! note
    This document includes features that are currently not available on the site, but are being planned. This will be removed!

* [x] Update Documentation
    * [x] About
    * [x] Meetings
    * [x] Qualifiers
    * [x] Other
* [x] Update `.gitlab-ci.yml` to work with MkDocs.
* [ ] Add Pictures
    * [ ] About
    * [ ] Meetings
    * [ ] Qualifiers
    * [ ] Other
* [x] Finish To-Do List

![alt text](../assets/Images/cover.jpg)

<small><small>Subscribe to PewDiePie.</small></small>
